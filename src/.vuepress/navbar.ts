import { navbar } from "vuepress-theme-hope";

export default navbar([
  "/",
  {
    text: "Projects",
    icon: "flask",
    prefix: "/projects/",
    children: [
      {
        text: "Web Sites",
        icon: "earth-americas",
        prefix: "websites/",
        children: [
          {
            text: "Upside Down",
            icon: "fa-brands fa-wordpress-simple",
            link: "updn.md",
          },
          {
            text: "Monroe Canyon RV",
            icon: "fa-brands fa-wordpress-simple",
            link: "monroerv.md",
          },
        ],
      },
      {
        text: "Technical",
        icon: "robot",
        prefix: "technical/",
        children: ["ray", { text: "...", icon: "ellipsis", link: "#" }],
      },
    ],
  },
]);
